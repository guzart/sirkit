import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';

// Initialize Firebase
const config = {
  apiKey: 'AIzaSyCuXxRhLjcxbYCwqOG7FmrqK3NlaHFgqYc',
  authDomain: 'sirkit-app.firebaseapp.com',
  databaseURL: 'https://sirkit-app.firebaseio.com',
  projectId: 'sirkit-app',
  storageBucket: 'sirkit-app.appspot.com',
  messagingSenderId: '951010892325',
};

firebase.initializeApp(config);

firebase.firestore().settings({ timestampsInSnapshots: true });

export default firebase;
