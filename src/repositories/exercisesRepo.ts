import fb from 'firebase/app';
import uuid from 'uuid/v1';
import Exercise from '@/models/Exercise';
import BaseRepository from '@/repositories/BaseRepository';
import Result from '@/lib/result/Result';
import { Validation, ValidationError } from '@/lib/validation';
import { isRequired } from '@/lib/validation/utils';

export type ExerciseId = string;

export interface AddExerciseImage {
  caption?: string;
  data: Blob;
  extension: string;
  id: string;
}

export interface AddExercise {
  name: string;
  description: string;
}

export type AddExerciseErrors = ValidationError<AddExercise>;

const validateDescription = isRequired({
  description: 'Please enter a description',
});

const validateName = isRequired({
  name: 'Please enter a name',
});

const validateAdd = (exercise: AddExercise) =>
  Validation.Success(exercise)
    .concat(validateName(exercise.name))
    .concat(validateDescription(exercise.description));

const buildExercise = (doc: fb.firestore.DocumentSnapshot) => {
  const data = doc.data();
  if (data == null || data.name == null || data.description == null) {
    throw new Error('Invalid exercise document');
  }

  return new Exercise(doc.id, data.name, [], data.description);
};

class ExercisesRepository extends BaseRepository {
  private get exercisesCollection() {
    return this.userDocument.collection('exercises');
  }

  public addExercise(
    data: AddExercise,
  ): Promise<Result<ExerciseId, AddExerciseErrors>> {
    const exerciseId = uuid();
    return validateAdd(data).matchWith({
      Failure: (value) => Promise.resolve(Result.Error(value)),
      Success: (validData) =>
        this.exercisesCollection
          .doc(exerciseId)
          .set(validData)
          .then(() => Result.Ok(exerciseId)),
    });
  }

  public updateExercise(
    id: ExerciseId,
    data: AddExercise,
  ): Promise<Result<void, AddExerciseErrors>> {
    return validateAdd(data).matchWith({
      Failure: (value) => Promise.resolve(Result.Error(value)),
      Success: (validData) =>
        this.exercisesCollection
          .doc(id)
          .update(validData)
          // .then(() => this.uploadImages(id, data.images))
          .then(() => Result.Ok(null)),
    });
  }

  public getAll() {
    return this.exercisesCollection.get().then((s) => {
      return s.docs.map(buildExercise);
    });
  }

  public get(id: ExerciseId) {
    return this.exercisesCollection
      .doc(id)
      .get()
      .then(buildExercise);
  }

  public destroy(id: ExerciseId) {
    return this.exercisesCollection.doc(id).delete();
  }

  public uploadImage(exerciseId: ExerciseId, image: AddExerciseImage) {
    const imageId = uuid();
    const imageRef = this.storage.ref(
      `/users/${this.userId}/exercise/${exerciseId}/${imageId}.${
        image.extension
      }`,
    );

    return imageRef.put(image.data);
  }
}

const exercisesRepo = new ExercisesRepository();
export default exercisesRepo;
