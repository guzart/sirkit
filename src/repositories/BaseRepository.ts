import firebase from '@/firebase';

export default class BaseRepository {
  protected get storage() {
    return firebase.storage();
  }

  protected get userId() {
    const user = firebase.auth().currentUser;
    if (!user) {
      throw new Error('Not authenticated');
    }

    return user.uid;
  }

  protected get userDocument() {
    return firebase
      .firestore()
      .collection('users')
      .doc(this.userId);
  }
}
