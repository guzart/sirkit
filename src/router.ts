import Vue from 'vue';
import Router from 'vue-router';

import Circuits from './views/Circuits.vue';
import ExerciseAdd from './views/ExerciseAdd.vue';
import ExerciseEdit from './views/ExerciseEdit.vue';
import Exercises from './views/Exercises.vue';
import LogIn from './views/LogIn.vue';
import Trainer from './views/Trainer.vue';
import firebase from './firebase';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  routes: [
    { path: '/', redirect: '/exercises' },
    {
      path: '/log-in',
      name: 'logIn',
      component: LogIn,
      meta: {
        requiresAuth: false,
      },
    },
    {
      path: '/circuits',
      name: 'circuits',
      component: Circuits,
    },
    {
      path: '/exercises',
      name: 'exercises',
      component: Exercises,
    },
    {
      path: '/exercises/add',
      name: 'addExercise',
      component: ExerciseAdd,
    },
    {
      path: '/exercises/:id/edit',
      name: 'editExercise',
      component: ExerciseEdit,
    },
    {
      path: '/trainer',
      name: 'trainer',
      component: Trainer,
    },
  ],
});

router.beforeEach((to, from, next) => {
  const requiresAuth = to.matched.some((record) => {
    if (firebase.auth().currentUser) {
      return false;
    }

    return record.meta.requiresAuth !== false;
  });

  if (requiresAuth) {
    next('/log-in');
  } else {
    next();
  }
});

export default router;
