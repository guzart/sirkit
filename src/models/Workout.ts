import Circuit from './Circuit';

interface Options {
  circuitRestTime?: number;
  exerciseRestTime?: number;
  exerciseTime?: number;
}

export default class Workout {
  public readonly circuitRestTime: number = 30;
  public readonly circuits: Circuit[];
  public readonly exerciseRestTime: number | null = null;
  public readonly exerciseTime: number | null = null;

  constructor(circuits: Circuit[], options?: Options) {
    this.circuits = circuits;

    if (options) {
      if (options.circuitRestTime) {
        this.circuitRestTime = options.circuitRestTime;
      }

      if (options.exerciseRestTime) {
        this.exerciseRestTime = options.exerciseRestTime;
      }

      if (options.exerciseTime) {
        this.exerciseTime = options.exerciseTime;
      }
    }
  }
}
