export default class ExerciseImage {
  public readonly path: string;

  constructor(path: string) {
    this.path = path;
  }
}
