import ExerciseImage from './ExerciseImage';

export default class Exercise {
  public readonly id: string;

  public readonly name: string;

  public readonly images: ExerciseImage[];

  public readonly description: string | null = null;

  constructor(
    id: string,
    name: string,
    images: ExerciseImage[],
    description?: string,
  ) {
    this.id = id;
    this.name = name;
    this.images = images;
    this.description = description || null;
  }
}
