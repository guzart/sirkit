import Exercise from './Exercise';

interface Options {
  exerciseRestTime?: number;
  exerciseTime?: number;
}

export default class Circuit {
  public exercises: Exercise[];
  public readonly exerciseRestTime: number = 10;
  public readonly exerciseTime: number = 30;

  constructor(exercises: Exercise[], options?: Options) {
    this.exercises = exercises;

    if (options) {
      if (options.exerciseRestTime) {
        this.exerciseRestTime = options.exerciseRestTime;
      }

      if (options.exerciseTime) {
        this.exerciseTime = options.exerciseTime;
      }
    }
  }
}
