type PopoverMethods =
  | 'show'
  | 'hide'
  | 'toggle'
  | 'dispose'
  | 'enable'
  | 'disable'
  | 'toggleEnabled'
  | 'update';

type PopoverFallbackPlacement = 'flip' | 'clockwise' | 'counterclockwise';

type PopoverOptions =
  | PopoverMethods
  | {
      animation?: boolean;
      boundary?: 'viewport' | 'window' | 'scrollParent' | HTMLElement;
      container?: string | HTMLElement | JQuery | false;
      content?: string | HTMLElement | JQuery | Function;
      delay?: number | { show: number; hide: number };
      fallbackPlacement?: PopoverFallbackPlacement | PopoverFallbackPlacement[];
      html?: boolean;
      offset?: number | string;
      placement?:
        | 'auto'
        | 'top'
        | 'bottom'
        | 'left'
        | 'right'
        | Function
        | string;
      selector?: string | false;
      template?: string;
      title?: string | HTMLElement | JQuery | Function;
      trigger?: 'click' | 'hover' | 'focus' | 'manual' | string;
    };

declare interface JQuery {
  popover(options?: PopoverOptions): JQuery;
}
