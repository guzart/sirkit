<template>
  <div class="mx-auto">
    <audio ref="audio"><source :src="soundEffect" type="audio/mpeg"></audio>
    <h1 v-if="isResting" class="text-center">Resting...</h1>
    <h1 v-else class="text-center">{{currentExercise.name}}</h1>
    <p class="display-3 text-center">
      {{secondsLeft | formatTime}}
    </p>
    <button v-if="isDone" @click="restart" class="btn btn-secondary btn-block">
      <i class="fas fa-sync-alt"></i>&nbsp;
      <strong class="text-uppercase">Again</strong>
    </button>
    <div v-else class="d-flex">
      <button v-if="isRunning" @click="pause" class="flex-grow-1 btn btn-secondary">
        <i class="fas fa-pause"></i>&nbsp;
        <strong class="text-uppercase">Pause</strong>
      </button>
      <button v-else @click="start" class="flex-grow-1 btn btn-primary">
        <i class="fas fa-play"></i>&nbsp;
        <strong class="text-uppercase">Start</strong>
      </button>
      <button @click="nextStep" class="btn btn-outline-dark ml-2">
        <strong class="text-uppercase">Skip</strong>
        <i class="ml-1 fas fa-chevron-double-right"></i>
      </button>
    </div>
    <div class="d-flex mt-4">
      <button @click="reset" class="flex-grow-1 btn btn-link btn-sm text-uppercase">
        <i class="fas fa-sync-alt"></i>
        Reset
      </button>
      <router-link to="/circuits" class="flex-grow-1 btn btn-link btn-sm text-uppercase">
        <i class="fas fa-door-open"></i>
        Exit
      </router-link>
    </div>
  </div>
</template>

<script lang="ts">
import moment from 'moment';
import Vue from 'vue';
import Workout from '@/models/Workout';
import Circuit from '@/models/Circuit';
import Exercise from '@/models/Exercise';
import exerciseFinishSound from '@/assets/sounds/retro_game/alert_002.mp3';
import exerciseStartSound from '@/assets/sounds/retro_game/success_004.mp3';
import circuitFinishSound from '@/assets/sounds/retro_game/level_complete_002.mp3';

const defaultWorkout = new Workout([
  new Circuit(
    [
      new Exercise('1', 'Sports Sprints', [], ''),
      new Exercise('2', 'Squats', [], ''),
      new Exercise('3', 'Crunches', [], ''),
      new Exercise('4', 'Worm Walkouts', [], ''),
      new Exercise('5', 'Push-Ups', [], ''),
      new Exercise('6', 'Alternating Supermans', [], ''),
      new Exercise('7', 'Shadow Boxing', [], ''),
      new Exercise('8', 'Skater Plyos', [], ''),
      new Exercise('9', 'Plank to Push-Up', [], ''),
      new Exercise('10', 'Step-Up Squats', [], ''),
    ],
    { exerciseTime: 30, exerciseRestTime: 10 },
  ),
]);

interface Data {
  currentCircuitIndex: number;
  currentExerciseIndex: number;
  finishesAt: moment.Moment;
  isDone: boolean;
  isResting: boolean;
  isRunning: boolean;
  secondsLeft: number;
  soundEffect: string;
  timeoutId: number;
  workout: Workout;
}

export default Vue.extend({
  name: 'Trainer',
  data(): Data {
    const workout = defaultWorkout;

    return {
      currentExerciseIndex: 0,
      currentCircuitIndex: 0,
      finishesAt: moment(),
      isDone: false,
      isResting: false,
      isRunning: false,
      secondsLeft: 0,
      soundEffect: exerciseFinishSound,
      timeoutId: 0,
      workout,
    };
  },
  mounted() {
    this.secondsLeft = this.exerciseTime;
  },
  beforeDestroy() {
    window.clearTimeout(this.timeoutId);
  },
  watch: {
    finishesAt() {
      this.updateTimer();
    },
    isRunning(current, old) {
      if (current) {
        this.timeoutId = window.setTimeout(() => this.updateTimer(), 40);
      }
    },
  },
  computed: {
    currentCircuit(): Circuit | null {
      return this.workout.circuits[this.currentCircuitIndex];
    },
    currentExercise(): Exercise | null {
      const { currentCircuit } = this;
      if (currentCircuit) {
        return currentCircuit.exercises[this.currentExerciseIndex];
      }

      return null;
    },
    exerciseTime(): number {
      const { currentCircuit, workout } = this;
      if (workout && workout.exerciseTime) {
        return workout.exerciseTime;
      }

      if (currentCircuit && currentCircuit.exerciseTime) {
        return currentCircuit.exerciseTime;
      }

      return 30;
    },
    exerciseRestTime(): number {
      const { currentCircuit, workout } = this;
      if (workout && workout.exerciseRestTime) {
        return workout.exerciseRestTime;
      }

      if (currentCircuit && currentCircuit.exerciseRestTime) {
        return currentCircuit.exerciseRestTime;
      }

      return 10;
    },
    nextExercise(): Exercise | null {
      const { currentCircuit } = this;
      if (currentCircuit) {
        return currentCircuit.exercises[this.currentExerciseIndex + 1];
      }

      return null;
    },
  },
  filters: {
    formatTime(value: number): string {
      let chars = Math.floor(value * 10).toString();
      if (value < 1) {
        chars = '0' + chars;
      }

      const lastIndex = chars.length - 1;
      return `${chars.slice(0, lastIndex)}.${chars.charAt(lastIndex)}`;
    },
  },
  methods: {
    nextStep() {
      if (!this.nextExercise) {
        this.isDone = true;
        this.isRunning = false;
        this.playSound(circuitFinishSound);
        return;
      }

      if (this.isResting) {
        this.isResting = false;
        this.finishesAt = moment().add(this.exerciseTime, 'seconds');
        this.currentExerciseIndex += 1;
        this.playSound(exerciseStartSound);
      } else {
        this.isResting = true;
        this.finishesAt = moment().add(this.exerciseRestTime, 'seconds');
        this.playSound(exerciseFinishSound);
      }
    },
    pause() {
      this.isRunning = false;
    },
    playSound(soundEffect: string) {
      const audioElement = this.$refs.audio as HTMLMediaElement;
      this.soundEffect = soundEffect;
      audioElement.load();
      window.requestAnimationFrame(() => audioElement.play());
    },
    reset() {
      this.isDone = false;
      this.currentExerciseIndex = 0;
      this.currentCircuitIndex = 0;

      this.isRunning = false;
      this.isResting = false;
      this.finishesAt = moment().add(this.exerciseTime, 'seconds');
    },
    restart() {
      this.reset();
      this.start();
    },
    start() {
      this.isRunning = true;
      this.finishesAt = moment().add(
        this.secondsLeft > 0 ? this.secondsLeft : this.exerciseTime,
        'seconds',
      );
    },
    updateTimer() {
      let secondsLeft = this.finishesAt.diff(moment(), 'milliseconds');
      if (secondsLeft <= 0) {
        secondsLeft = 0;
        this.nextStep();
      }

      this.secondsLeft = Math.round(secondsLeft / 100.0) / 10.0;
      if (this.isRunning) {
        this.timeoutId = window.setTimeout(() => this.updateTimer(), 40);
      }
    },
  },
});
</script>
