export interface UserInfo {
  displayName: string | null;
  email: string | null;
  phoneNumber: string | null;
  photoURL: string | null;
  providerId: string;
  uid: string;
}

export default interface Store {
  title: string;
  userInfo: null | UserInfo;
}
