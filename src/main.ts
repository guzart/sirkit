import Vue from 'vue';
import 'bootstrap/js/src/popover';

import App from './App.vue';
import AppShell from './components/AppShell.vue';
import ConfirmButton from '@/components/ConfirmButton.vue';
import FormInput from './components/FormInput.vue';
import LoaderButton from './components/LoaderButton.vue';
import LoaderMessage from './components/LoaderMessage.vue';
import PopoverHelp from './components/PopoverHelp.vue';
import firebase from './firebase';
import router from './router';
import store from './store';
import './directives/focus';
import './directives/popover';

import './registerServiceWorker';

Vue.config.productionTip = false;
Vue.component('app-shell', AppShell);
Vue.component('confirm-button', ConfirmButton);
Vue.component('form-input', FormInput);
Vue.component('loader-button', LoaderButton);
Vue.component('loader-message', LoaderMessage);
Vue.component('popover-help', PopoverHelp);

firebase.auth().onAuthStateChanged((userInfo) => {
  if (userInfo) {
    store.commit('setUserInfo', userInfo);
  }

  new Vue({
    router,
    store,
    render: (h) => h(App),
  }).$mount('#app');
});
