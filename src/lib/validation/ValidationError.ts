export type ValidationError<T> = { base?: string } & {
  [key in keyof T]?: string
};
