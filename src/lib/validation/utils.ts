import { isEmpty } from 'lodash';
import Validation from '@/lib/validation/Validation';

export const isRequired = <T>(validationErrors: T) => (value: string) =>
  isEmpty(value) ? Validation.Failure(validationErrors) : null;
