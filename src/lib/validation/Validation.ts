import { ValidationError } from '@/lib/validation/ValidationError';

export default abstract class Validation<
  S extends { [key: string]: any } = { [key: string]: any },
  F extends ValidationError<S> = ValidationError<S>
> {
  public static Failure<S, F extends ValidationError<S> = ValidationError<S>>(
    value: F,
  ) {
    return new Failure(value);
  }

  public static Success<S>(value: S) {
    return new Success(value);
  }

  public static fromNullable<S>(value: S | null) {
    return value != null
      ? this.Success(value)
      : this.Failure<S>({} as { [key in keyof S]: string });
  }

  public static of<S>(value: S) {
    return this.Success(value);
  }

  public static isFailure(validation: Validation): validation is Failure {
    return validation.isFailure;
  }

  public static isSuccess(validation: Validation): validation is Success {
    return validation.isSuccess;
  }

  public isFailure = false;

  public isSuccess = false;

  public abstract concat(
    validation: null | Validation<Partial<S>, F>,
  ): Validation<S, F>;

  public abstract matchWith<X, Y>(patterns: {
    Success: (value: S) => X;
    Failure: (value: F) => Y;
  }): X | Y;
}

// tslint:disable-next-line:max-classes-per-file
export class Success<
  S extends { [key: string]: any } = { [key: string]: any },
  F extends ValidationError<S> = ValidationError<S>
> extends Validation<S, F> {
  public isSuccess = true;

  public readonly value: S;

  constructor(value: S) {
    super();
    this.value = value;
  }

  public concat(validation: null | Validation<Partial<S>>): Validation<S> {
    if (validation != null && validation instanceof Failure) {
      return validation;
    }

    return this;
  }

  public matchWith<X, Y>(patterns: {
    Success: (value: S) => X;
    Failure: (value: F) => Y;
  }): X | Y {
    return patterns.Success(this.value);
  }
}

// tslint:disable-next-line:max-classes-per-file
export class Failure<
  S extends { [key: string]: any } = { [key: string]: any },
  F extends ValidationError<S> = ValidationError<S>
> extends Validation<any, F> {
  public isFailure = true;

  public readonly value: F;

  constructor(value: F) {
    super();
    this.value = value;
  }

  public concat(
    validation: null | Validation<Partial<S>, F>,
  ): Validation<S, F> {
    if (validation != null && validation instanceof Failure) {
      const nextFailure = Object.assign(this.value, validation.value);
      return new Failure(nextFailure);
    }

    return this;
  }

  public matchWith<X, Y>(patterns: {
    Success: (value: any) => X;
    Failure: (value: F) => Y;
  }): X | Y {
    return patterns.Failure(this.value);
  }
}
