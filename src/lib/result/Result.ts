export default abstract class Result<O, E> {
  public static Error<E>(value: E) {
    return new Error(value);
  }

  public static Ok<O>(value: O) {
    return new Ok(value);
  }

  public static fromNullable<O>(value: O | null) {
    return value != null ? this.Ok(value) : this.Error(null);
  }

  public static of<O>(value: O) {
    return this.Ok(value);
  }

  public static isError<O, E>(result: Result<O, E>): result is Error<E> {
    return result instanceof Error;
  }

  public static isOk<O, E>(result: Result<O, E>): result is Ok<O> {
    return result instanceof Ok;
  }
}

// tslint:disable-next-line:max-classes-per-file
export class Ok<O> extends Result<O, any> {
  public readonly value: O;

  public constructor(value: O) {
    super();
    this.value = value;
  }
}

// tslint:disable-next-line:max-classes-per-file
export class Error<E> extends Result<any, E> {
  public readonly value: E;

  public constructor(value: E) {
    super();
    this.value = value;
  }
}
