import Vue from 'vue';
import Vuex from 'vuex';
import Store from '@/types/Store';

Vue.use(Vuex);

export default new Vuex.Store<Store>({
  state: {
    title: 'Sirkit',
    userInfo: null,
  },
  mutations: {
    changeTitle(state, title: string) {
      state.title = title;
    },
    setUserInfo(state, user: firebase.UserInfo) {
      state.userInfo = user;
    },
  },
});
