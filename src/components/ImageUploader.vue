<template>
  <div :id="instanceId" class="image-uploader" :class="classList"
      @drop="filesDropped" @dragover="dragOver" @dragleave="dragLeave">
    <p v-if="files.length === 0" class="mb-0 text-center">
      <span>Drop files here or </span>
      <button type="button" class="btn btn-link" @click="selectFiles">select files.</button>
      <input ref="fileInput" title="File input" multiple @input="filesSelected" type="file" :name="instanceId" />
    </p>
    <p v-if="files.length === 0 && maxFileSize > 0" class="text-center text-muted">
      <small>(Max. size {{maxFileSize | humanFileSize}})</small>
    </p>
    <div v-else class="card-deck">
      <div class="card" v-for="file in files" :key="file.id">
        <img class="card-img-top img-fluid" :src="file.previewDataUrl" :alt="file.name" />
        <div class="card-body d-flex flex-column">
          <span class="mb-2">
            <strong>{{file.name}}</strong>
            <small class="ml-2 text-secondary">{{file.size | humanFileSize}}</small>
          </span>
          <div class="d-flex mt-auto card-buttons">
            <confirm-button
              label="Remove"
              confirm-label="Remove"
              confirm-message="Remove this image from the upload list?"
              :show-icon="false"
              :show-label="true"
              v-on:confirm="remove(file.id)"
            />
          </div>
        </div>
      </div>
    </div>
  </div>
</template>

<script lang="ts">
import uuid from 'uuid/v1';
import Vue from 'vue';
import { AddExerciseImage } from '@/repositories/exercisesRepo';

const PREVIEW_MAX_WIDTH = 286;
const PREVIEW_MAX_HEIGHT = 180;

function humanFileSize(bytes: number) {
  let size = bytes;
  let sizeCode = '';
  ['KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'].forEach((code) => {
    if (size / 1024 < 1) {
      return;
    }

    size = size / 1024;
    sizeCode = code;
  });

  return `${size.toFixed(1)} ${sizeCode}`;
}

const IMAGE_TYPE_REGEX = /^image\//i;

function isImage(type: string) {
  return IMAGE_TYPE_REGEX.test(type);
}

function validateFile(file: File) {
  return isImage(file.type);
}

function resizeImage(imageDataUrl: string): Promise<string> {
  const canvas = document.createElement('canvas');
  const ctx = canvas.getContext('2d');
  if (!ctx) {
    return Promise.resolve(imageDataUrl);
  }

  return new Promise((resolve, reject) => {
    const img = document.createElement('img');
    img.src = imageDataUrl;
    img.onload = () => {
      ctx.drawImage(img, 0, 0);

      let width = img.width;
      let height = img.height;
      if (width > height && width > PREVIEW_MAX_WIDTH) {
        height = Math.round(height * PREVIEW_MAX_WIDTH / width);
        width = PREVIEW_MAX_WIDTH;
      } else if (height > PREVIEW_MAX_HEIGHT) {
        width = Math.round(width * PREVIEW_MAX_HEIGHT / height);
        height = PREVIEW_MAX_HEIGHT;
      }

      canvas.width = width;
      canvas.height = height;

      const outputCtx = canvas.getContext('2d');
      if (outputCtx) {
        outputCtx.drawImage(img, 0, 0, width, height);
        resolve(canvas.toDataURL('image/png'));
      } else {
        resolve(imageDataUrl);
      }
    };
  });
}

function buildImagePreview(file: File): Promise<string> {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);

    reader.onabort = (event: ProgressEvent) => {
      reject(reader.error || 'Aborted');
    };

    reader.onerror = (event: ProgressEvent) => {
      reject(reader.error || 'Error');
    };

    reader.onload = (event: ProgressEvent) => {
      if (typeof reader.result === 'string') {
        const output = resizeImage(reader.result);
        resolve(output);
      } else {
        reject('Invalid image type');
      }
    };
  });
}

// TODO:
//   * Design for all invalid state
//   * Design for upload states

interface UploaderFile {
  id: string;
  isImage: boolean;
  isValid: boolean;
  previewDataUrl: string | null;
  name: string;
  source: File;
  size: number;
}

export interface FileUploadStatus {
  errorMessage: string | null;
  id: string;
  status: 'idle' | 'queued' | 'progress' | 'done' | 'error';
  uploadProgress: number;
}

export interface FilesUploadStatus {
  [key: string]: FileUploadStatus;
}

function processFile(file: File) {
  const output: UploaderFile = {
    id: uuid(),
    isImage: isImage(file.type),
    isValid: validateFile(file),
    name: file.name,
    previewDataUrl: null,
    size: file.size,
    source: file,
  };

  if (output.isImage) {
    buildImagePreview(file).then((result) => {
      output.previewDataUrl = result;
    });
  }

  return output;
}

export default Vue.extend({
  name: 'ImageUploader',
  filters: {
    humanFileSize,
  },
  props: {
    id: {
      type: String,
      required: true,
    },
    maxFileSize: {
      type: Number,
      default: 0,
    },
    uploadStatus: {
      type: Object,
      required: true,
    },
  },
  data() {
    return {
      hasFilesOver: false,
      files: [] as UploaderFile[],
    };
  },
  computed: {
    classList(): any {
      return {
        '--hover': this.hasFilesOver,
      };
    },
    instanceId(): string {
      return `image-uploader-${this.id}`;
    },
  },
  methods: {
    dragOver(event: DragEvent) {
      // Prevent browser's default behaviour
      event.preventDefault();
      this.hasFilesOver = true;
    },
    dragLeave() {
      this.hasFilesOver = false;
    },
    filesSelected(event: Event) {
      const fileInput = this.$refs.fileInput;
      if (fileInput instanceof Element) {
        const input = fileInput as HTMLInputElement;
        if (input.files) {
          const files = Array.from(input.files).map(processFile);
          this.files = this.files.concat(files);
        }
      }
    },
    filesDropped(event: DragEvent) {
      // Files have been dropped, so they are not over 🙃
      this.hasFilesOver = false;

      // Also prevent browser's default behaviour
      event.preventDefault();

      // Processing
      const files = Array.from(event.dataTransfer.files).map(processFile);
      this.files = this.files.concat(files);

      // Cleanup
      event.dataTransfer.clearData();
    },
    remove(id: string) {
      this.files = this.files.filter((f) => f.id !== id);
    },
    selectFiles() {
      const fileInput = this.$refs.fileInput;
      if (fileInput instanceof Element) {
        (fileInput as HTMLInputElement).click();
      }
    },
  },
});
</script>

<style lang="scss">
@import '@/styles/base.scss';

.image-uploader {
  background-color: $gray-100;
  border: 1px dashed $border-color;
  border-radius: $border-radius;
  min-height: 150px;
  padding: 20px;

  .btn-link {
    display: inline;
    padding: 0;
  }

  input {
    cursor: pointer;
    opacity: 0;
    position: absolute;
    right: 0px;
    top: -10000px;
  }

  &.--hover {
    border-color: $primary;
  }

  .card {
    max-width: calc(50% - #{$card-deck-margin * 2});
  }
}
</style>

