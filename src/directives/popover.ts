import $ from 'jquery';
import Vue, { DirectiveOptions } from 'vue';

Vue.directive('popover', {
  bind(el, binding) {
    $(el).attr('ref', binding.value);
  },
} as DirectiveOptions);
