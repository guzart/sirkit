import Vue from 'vue';

Vue.directive('focus', {
  inserted(el, binding) {
    if (String(binding.value) === 'true') {
      window.requestAnimationFrame(() => el.focus());
    }
  },
});
